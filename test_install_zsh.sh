#!/bin/bash

if [[ "$(echo $SHELL)" == *"zsh"* ]]; then
    echo "zsh is the shell"
else
    echo "zsh is not the shell";
    exit 1;
fi
