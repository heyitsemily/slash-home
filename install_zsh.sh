#!/bin/bash

echo "slash-home installer"
echo ""

if [ ! "$BASH_VERSION" ]; then
	echo "Error: installer must be run from bash";
	exit 1;
fi

if [[ -z "$(which zsh)" ]]; then
    sudo apt install -y zsh
fi

chsh -s $(which zsh)
